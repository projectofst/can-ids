#ifndef __IIB_H__
#define __IIB_H__

/*Can interface for IIB*/

#include <stdint.h>
#include "../CAN_IDs.h"

/*Message IDs*/



typedef struct{
	uint16_t FL;
	uint16_t FR;
	uint16_t RL;
	uint16_t RR; //for each inverter
}IIB_speed;

#define MSG_ID_IIB_ERROR 33

typedef struct{
	uint16_t FL;
	uint16_t FR;
	uint16_t RL;
	uint16_t RR; //for each inverter
}IIB_error;

#define MSG_ID_IIB_VALUES 34

typedef struct{
	uint16_t node; //0-FL; 1-FR; 2-RL; 3-RR; identifies the message
	uint16_t AMK_temp_motor;
	uint16_t AMK_temp_inverter;
	uint16_t AMK_temp_IGBT;
	uint16_t torque;
	uint16_t magnetizing_current; 
}IIB_values;

#define MSG_ID_IIB_STATUS 35

typedef struct{
	unsigned INV_RTR       :1;
    unsigned CAR_RTR       :1;
    unsigned INV_ERROR_1   :1;
    unsigned INV_ERROR_2   :1;
    unsigned INV_ERROR_3   :1;
    unsigned INV_ERROR_4   :1;
    unsigned INV_P_ERROR_1 :1;
    unsigned INV_P_ERROR_2 :1;
    unsigned INV_P_ERROR_3 :1;
    unsigned INV_P_ERROR_4 :1;
    unsigned SEND_ERROR    :1;
    unsigned REGEN_OK      :1;
    unsigned TURNING_OFF   :1;
    unsigned TURNING_ON    :1;
    unsigned INV_HV        :1;
    unsigned SDC1          :1;
    unsigned SDC2          :1;
    unsigned CONTROLLER    :1;
    unsigned I1            :1;
    unsigned I2            :1;
    unsigned I3            :1;
    unsigned I4            :1;
    unsigned REAR_OFF      :1;
    unsigned FRONT_OFF     :1;
}IIB_status; 

//CAN structure

#define MSG_ID_IIB_LIMS 36

typedef struct{
    uint16_t lim_rpm;
    uint16_t lim_torque_f;
    uint16_t lim_torque_r;
}IIB_lims;

typedef struct{
	IIB_speed speed;
	IIB_error error;
	IIB_values values;
	IIB_status status;
	IIB_lims lims;
}IIB_CAN_data; 


void parse_IIB_speed_message(uint16_t data[4], IIB_speed *speed);
void parse_IIB_error_message(uint16_t data[4], IIB_error *error);
void parse_IIB_values_message(uint16_t data[4], IIB_values *values);
void parse_IIB_status_message(uint16_t data[2], IIB_status *status);
void parse_IIB_lims_message(uint16_t data[4], IIB_lims *lims);
void parse_can_IIB(CANdata message, IIB_CAN_data *data);

#endif
