#ifndef __CCU_CAN_H__
#define __CCU_CAN_H__

#include <stdint.h>

#include "../CAN_IDs.h"


#define MSG_ID_CCU_RESET						40					
#define MSG_ID_CCU_PWM_FANS						50
#define MSG_ID_CCU_PWM_PUMP						51


#endif