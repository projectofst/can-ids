#include "STEER_CAN.h"

void parse_can_steer(CANdata message, STEER_MSG_SIG *steer){

	if (message.dev_id != DEVICE_ID_STEER) {
		/*FIXME: send info for error logging*/
        return;
	}
	else {
		steer->time_stamp = message.data[0];
		steer->value = (int16_t) message.data[1];

		if(message.dlc == 6){
			steer->clear_interface = (bool) message.data[2];
		}

	}
}
