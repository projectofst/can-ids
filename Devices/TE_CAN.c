#include <stdbool.h>
#include <stdint.h>
#include "can-ids/CAN_IDs.h"
#include "TE_CAN.h"

void parse_te_main_message(uint16_t data[4], TE_MESSAGE *main_message)
{
	main_message->status.TE_status	= data [0];
	main_message->APPS				= data [1];
	main_message->BPS_pressure		= data [2];
	main_message->BPS_electric		= data [3];

	return;
}


/*
void parse_te_debug_message(uint16_t data[4], TE_DEBUG_MESSAGE *debug_message)
{
	debug_message->APPS_0			= data [0];
	debug_message->APPS_1			= data [1];
	debug_message->BPS_pressure_0	= data [2];
	debug_message->BPS_pressure_1	= data [3];

	return;
}
*/

void parse_te_verbose_1_message(uint16_t data[4], TE_VERBOSE_MESSAGE *verbose_message){

	verbose_message->apps_0 	= data[0];
	verbose_message->apps_1 	= data[1];
	verbose_message->pressure_0 = data[2];
	verbose_message->pressure_1 = data[3];

	return;
}



void parse_te_verbose_2_message(uint16_t data[4], TE_VERBOSE_MESSAGE *verbose_message){

	verbose_message->electric = data[0];
	verbose_message->time_imp = data[1];
	verbose_message->apps_imp_flag = data[2];
	verbose_message->apps_timer_exceed_flag=data[3];

	return;
}


void parse_can_te_limit(CANdata message, TE_LIMIT *limit_message){
	limit_message->header = message.data[0];
	limit_message->data_limit= message.data[1];
}



void parse_can_te(CANdata message, TE_CAN_Data *data)
{
	if (message.dev_id != DEVICE_ID_TE) {
		return;
	}

	switch (message.msg_id) {
		case MSG_ID_TE_MAIN:
			parse_te_main_message(message.data, &(data->main_message));
			break;
		//case MSG_ID_TE_DEBUG:
			// parse_te_debug_message(message.data, &(data->debug_message));
			// break;
		case MSG_ID_TE_VERBOSE_1:
			parse_te_verbose_1_message(message.data, &data->verbose_message);
			break;
		case MSG_ID_TE_VERBOSE_2:
			parse_te_verbose_2_message(message.data, &data->verbose_message);
			break;
        case MSG_ID_TE_LIMIT_ANSWER:
			parse_can_te_limit(message, &(data->limit_message));
	}
	return;
}
