#include "can-ids/Devices/SUPOT_CAN.h"

void parse_can_supot_sig (CANdata message, SUPOT_MSG_SIG *supots_values){

    if(message.dev_id == DEVICE_ID_SUPOT_FRONT){

        supots_values->front_right = message.data[0];
        supots_values->front_left  = message.data[1];

    }else if(message.dev_id == DEVICE_ID_SUPOT_REAR){

        supots_values->rear_right = message.data[0];
        supots_values->rear_left  = message.data[1];

    }
    return;
}